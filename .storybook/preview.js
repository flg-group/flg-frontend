import React from 'react'
import styled from 'styled-components'

import { theme } from 'theme'
import { Main } from 'ui/atoms/main'

import 'features/error/lib/logger/console'
import 'index.css'

const getLabelAndValueFromObj = (obj) =>
  Object.keys(obj).reduce((acc, key) => [...acc, { name: key, value: obj[key] }], [])

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  color: getLabelAndValueFromObj(theme.color),
  fontSize: getLabelAndValueFromObj(theme.font.size),
}

const Container = styled(Main)`
  justify-content: center;
  align-items: center;
`

const everyStoryDecorator = (Story) => (
  <Container>
    <Story />
  </Container>
)

export const decorators = [everyStoryDecorator]
