module.exports = {
  stories: ['../src/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: ['@storybook/addon-essentials', '@storybook/preset-create-react-app'],
  //disable eslint warnings in storybook mode
  webpackFinal: (config) => {
    return {
      ...config,
      module: {
        rules: config.module.rules.filter((rule) => {
          if (!rule.use) return true
          return !rule.use.find(
            (useItem) => typeof useItem.loader === 'string' && useItem.loader.includes('eslint-loader')
          )
        }),
      },
    }
  },
}
