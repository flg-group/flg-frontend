import initStoryshots from '@storybook/addon-storyshots'
import { imageSnapshot } from '@storybook/addon-storyshots-puppeteer'

const baseScreenshotsPath = '__screenshots__/__stories__'

const getMatchOptions = () => {
  return {
    customSnapshotsDir: `${baseScreenshotsPath}`,
    customDiffDir: `${baseScreenshotsPath}/__diffs__`,
  }
}

//initializating screenshot visual regression with all storybook stories
initStoryshots({ suite: 'Puppeteer storyshots', test: imageSnapshot({ getMatchOptions }) })
