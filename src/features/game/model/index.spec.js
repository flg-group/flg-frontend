import { fork, root } from 'effector-root'
import { bindCallAction } from 'lib/bind-scope-action'
import { GAME_STATE_RAW } from 'mocks/state'

import {
  $leverageOn,
  $mortgageOn,
  $selectedSubTabData,
  resetLeverage,
  resetMortgage,
  setGameState,
  toggleLeverage,
  toggleMortgage,
} from '.'
import { setSelectedSubTab } from '../navigation/model'
import { InstrumentTypes } from '../types'

describe('game field', () => {
  const scope = fork(root)
  const callAction = bindCallAction(scope)
  beforeEach(async () => {
    callAction(setGameState, GAME_STATE_RAW)
    callAction(resetLeverage)
    callAction(resetMortgage)
  })
  test('should turn off mortgage when subtab switched from RE to stocks', async () => {
    callAction(setSelectedSubTab, InstrumentTypes.RealEstate)
    callAction(toggleMortgage)
    expect(scope.getState($mortgageOn)).toBeTruthy()
    callAction(setSelectedSubTab, InstrumentTypes.Stock)
    expect(scope.getState($mortgageOn)).toBeFalsy()
  })

  test('should turn off leverage when subtab switched from stocks to RE', async () => {
    callAction(setSelectedSubTab, InstrumentTypes.Stock)
    callAction(toggleLeverage)
    expect(scope.getState($leverageOn)).toBeTruthy()
    callAction(setSelectedSubTab, InstrumentTypes.RealEstate)
    expect(scope.getState($leverageOn)).toBeFalsy()
  })
})
