import React from 'react'
import { render } from '@testing-library/react'
import { root, fork } from 'effector-root'

import { GAME_STATE_RAW } from 'mocks/state'
import { $profile, $selectedInstrument, makeTransaction, setGameState } from 'features/game/model'
import { setSelectedSubTab } from 'features/game/navigation/model'
import { $state, setSelectedInstrument } from 'features/game/model/state'
import { InstrumentTypes } from 'features/game/types'
import { generateInputEvent } from 'lib/generate-input-event'
import { bindCallAction } from 'lib/bind-scope-action'
import { generateClickEvent } from 'lib/generate-click-event'

import { BuySellTab } from '.'

describe('buy sell tab', () => {
  const makeTransactionMock = jest.fn()
  const scope = fork(root, { handlers: new Map().set(makeTransaction, makeTransactionMock) })
  const callAction = bindCallAction(scope)
  let secondStockItem

  beforeEach(async () => {
    callAction(setGameState, GAME_STATE_RAW)
    secondStockItem = scope.getState($state).financialMarkets.stocks[2]
  })

  test('cant buy stocks more than money you have', async () => {
    callAction(setSelectedSubTab, InstrumentTypes.Stock)
    callAction(setSelectedInstrument, secondStockItem)
    const selected = scope.getState($selectedInstrument)
    const profile = scope.getState($profile)
    const { getByTestId } = render(<BuySellTab selectedInstrument={selected} {...{ profile }} />)
    generateInputEvent(getByTestId('buy-sell-qty-input'), 2)
    const buyButton = getByTestId('buy-sell-buy-button')
    expect(buyButton).toHaveAttribute('disabled')
  })

  test('can buy stocks if you have enough money', async () => {
    callAction(setSelectedSubTab, InstrumentTypes.Stock)
    callAction(setSelectedInstrument, secondStockItem)
    const selected = scope.getState($selectedInstrument)
    const profile = scope.getState($profile)
    const { getByTestId } = render(<BuySellTab selectedInstrument={selected} {...{ profile }} />)
    generateInputEvent(getByTestId('buy-sell-qty-input'), 1)
    const buyButton = getByTestId('buy-sell-buy-button')
    generateClickEvent(buyButton)
    expect(buyButton).not.toHaveAttribute('disabled')
  })
})
