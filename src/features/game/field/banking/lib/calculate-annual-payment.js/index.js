export const calcAnnualPayment = (sum, years, rate) => {
  if (rate === 0) return sum / years
  const multiplier = Math.pow(1 + rate, years)
  return (sum * rate * multiplier) / (multiplier - 1)
}
