import styled from 'styled-components'
import { ifProp } from 'styled-tools'

export const TabInnerSection = styled.section`
  padding: 11px;
  ${ifProp('small', 'padding: 8px;')}
  ${ifProp('noPt', 'padding-top: 0;')}
`
