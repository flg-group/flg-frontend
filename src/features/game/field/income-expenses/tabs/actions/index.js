import React from 'react'
import { FINANCIAL_SETTINGS_ID } from 'features/game/lib/normalize-market/add-income-expenses'
import { NoData } from 'ui/molecules/no-data'
import { FinancialSettings } from './financial-settings'

export const ExpensesActionTab = ({ selectedInstrument }) => {
  return selectedInstrument?.id === FINANCIAL_SETTINGS_ID ? <FinancialSettings {...{ selectedInstrument }} /> : <NoData />;
}
