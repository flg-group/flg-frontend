import { getSubjectInstrument } from '../get-subject-instrument'

export const getFutureValues = (profile, selectedInstrument, key = 'points') => {
  let obj = {
    current: {
      endBonus: profile.profile.balance.endBonus.current,
      flowBonus: profile.profile.balance.flowBonus.current,
    },
    instrument: {
      endBonus: selectedInstrument.bonuses.buying.endBonus,
      flowBonus: selectedInstrument.bonuses.buying.flowBonus,
      price: selectedInstrument.price.current,
    },
    relatedTask: {
      endBonus: 0,
      flowBonus: 0,
    },
  }
  if (selectedInstrument.relatedGoalID !== null) {
    const relatedTask = getSubjectInstrument(selectedInstrument.relatedGoalID, 'goals')
    if (relatedTask?.flowBonus) obj.relatedTask.flowBonus += relatedTask.flowBonus
    if (relatedTask?.endBonus) obj.relatedTask.endBonus += relatedTask.endBonus
  }
  if (selectedInstrument.relatedDreamID !== null) {
    const relatedTask = getSubjectInstrument(selectedInstrument.relatedDreamID, 'dreams')
    if (relatedTask?.flowBonus) obj.relatedTask.flowBonus += relatedTask.flowBonus
    if (relatedTask?.endBonus) obj.relatedTask.endBonus += relatedTask.endBonus
  }
  let total = {
    flowBonus: obj.current.flowBonus + obj.instrument.flowBonus + obj.relatedTask.flowBonus,
    endBonus: obj.current.endBonus + obj.instrument.endBonus + obj.relatedTask.endBonus,
  }
  total.points = obj.instrument.price * total.flowBonus * total.endBonus
  return total[key]
}
