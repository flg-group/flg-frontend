import { getStatusDelta } from '.'

describe('get status and delta', () => {
  test('return is correct for up situation', () => {
    expect(getStatusDelta(8, 4)).toEqual({
      status: 'up',
      delta: '+100%',
      deltaAbs: '+4',
      deltaSign: '+',
      deltaAbsNumber: 4,
      deltaNumber: 100,
    })
  })
  test('return is correct for down situation', () => {
    expect(getStatusDelta(27216, 32400)).toEqual({
      status: 'down',
      delta: '-16%',
      deltaAbs: '-5 184',
      deltaAbsNumber: 5184,
      deltaNumber: 16,
      deltaSign: '-',
    })
  })
  test.todo('return is correct with float nums')
  test.todo('return is correct for no data from prev')
  test('return is correc for neutral situation', () => {
    expect(getStatusDelta(1, 1)).toEqual({
      status: 'neutral',
      delta: '',
      deltaAbs: '',
      deltaAbsNumber: null,
      deltaNumber: null,
      deltaSign: '',
    })
  })
})
