import { formatPrice } from './index.ts'

test('price formats number input correctly', () => {
  expect(formatPrice(3650230)).toBe('$ 3 650 230')
})
test('price formats string input correctly', () => {
  expect(formatPrice('3650230')).toBe('$ 3 650 230')
})
test('price formats string input with floats without any decimals', () => {
  expect(formatPrice('3650230.344300')).toBe('$ 3 650 230')
})
test('price formats number input with floats without any decimals', () => {
  expect(formatPrice(3650230.3443)).toBe('$ 3 650 230')
})
test('price formats string input with zero floats correctly', () => {
  expect(formatPrice('3650230.0000')).toBe('$ 3 650 230')
})
test('price formats number input with zero floats correctly', () => {
  expect(formatPrice(3650230)).toBe('$ 3 650 230')
})
test('price formats negative number input with floats correctly', () => {
  expect(formatPrice(-3650230.3443)).toBe('$ -3 650 230.34')
})
test('price formats negative string input with floats correctly', () => {
  expect(formatPrice('-3650230.3443')).toBe('$ -3 650 230.34')
})
test('price formats without currency correctly', () => {
  expect(formatPrice('-3650230.3443', { disableCurrency: true })).toBe('-3 650 230.34')
})
test('price formats with forced decimals correctly with 1 decimal', () => {
  expect(formatPrice('-3650230.3', { forceDecimals: true })).toBe('$ -3 650 230.30')
})
test('price formats with forced decimals correctly with 2 decimals', () => {
  expect(formatPrice('-3650230.34', { forceDecimals: true })).toBe('$ -3 650 230.34')
})
test('price formats with forced decimals correctly with 0 decimals', () => {
  expect(formatPrice('-3650230', { forceDecimals: true })).toBe('$ -3 650 230.00')
})
test('price formats with forced decimals correctly with 1 decimals and 1 zero after dot', () => {
  expect(formatPrice('-3650230.30', { forceDecimals: true })).toBe('$ -3 650 230.30')
})
test('price formats without no decimals prop correctly', () => {
  expect(formatPrice('-3650230.3443', { noDecimals: true })).toBe('$ -3 650 230')
})
test('price formats deletes decs with no decimals prop and pastes two zeros correctly', () => {
  expect(formatPrice('-3650230.3443', { noDecimals: true, forceDecimals: true })).toBe('$ -3 650 230.00')
})
test('price is negative and near zero leads to -0', () => {
  expect(formatPrice('-0.15')).toBe('$ -0')
})
test('thousand price simplifies correct, without ceiling 2.4', () => {
  expect(formatPrice('-2405', { simplify: true })).toBe('$ -2k')
})
test('million price simplifies correct with ceiling to 3m', () => {
  expect(formatPrice('2906782.34', { simplify: true })).toBe('$ 3m')
})
