import { InstrumentTypes } from 'features/game/types'

export const filterMarket = (list) => list.filter((elem) => !elem.isPortfolio)
export const filterPortfolio = (list) => list.filter((elem) => elem.isPortfolio)
export const filterNotReady = (list) => list.filter((elem) => elem.status !== 'ready')
export const filterMarketLeverage = function ff(list) {
  return list.filter((elem) => (elem.instrumentType === InstrumentTypes.Leverage ? elem.isPortfolio : true))
}
