import { formatPrice } from '../prices/format'

export const formatToPercent = (data) => `${formatPrice(data, { disableCurrency: true })}%`
export const formatTooltipYMoney = (data) => formatPrice(data)
export const formatTooltipYPoints = (data) => formatPrice(data, { disableCurrency: true })
export const round = (data) => formatPrice(data, { disableCurrency: true, noDecimals: true, simplify: true })
