import styled from 'styled-components'

export const Tab = styled.div`
  flex: 1;
  height: 100%;
`
