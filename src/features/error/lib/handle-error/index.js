import { addError } from '../../model'

export const handleUnhandledError = (e) => {
  window.log(e.toString(), 'error handled')
  let error = e.error
  if (e instanceof Error) {
    error = e
  }
  let level = e.level ? e.level : 'error'
  const FATAL_ERROR_TYPES = ['TypeError', 'RangeError', 'ReferenceError', 'SyntaxError', 'EvalError']
  if (FATAL_ERROR_TYPES.some((type) => error?.name === type)) {
    level = 'fatal'
  }
  addError({ level, error })
}
