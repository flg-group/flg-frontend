import React from 'react'
import { render, act, cleanup } from '@testing-library/react'
import { waitFor } from '@testing-library/dom'
import userEvent from '@testing-library/user-event'

import { registerUserFetching } from 'features/auth/model'

import { RegisterForm } from '.'
import { generateInputEvent } from 'lib/generate-input-event'

beforeEach(cleanup)

describe('register form', () => {
  const { click, type } = userEvent
  test('call registerUser if everything is correct', async () => {
    const registerUserMock = jest.fn()
    const { getByText, getByPlaceholderText } = render(
      <RegisterForm onSubmitForm={registerUserMock} fetching={registerUserFetching} />
    )
    type(getByPlaceholderText('Email'), 'test@test.test')
    type(getByPlaceholderText('Password'), 'testtest')
    click(getByText('Sign Up'))
    await waitFor(() => expect(registerUserMock).toHaveBeenCalled())
  })

  test('should not call registerUser if form is not filled/partly filled', async () => {
    const registerUserMock = jest.fn()
    const { getByRole, getByPlaceholderText } = render(
      <RegisterForm onSubmitForm={registerUserMock} fetching={registerUserFetching} />
    )
    generateInputEvent(getByPlaceholderText('Email'), 'test@test.test')
    await act(async () => {
      click(getByRole('button'))
    })
    expect(getByRole('alert')).toBeInTheDocument()
    expect(registerUserMock).not.toBeCalled()
  })
})
