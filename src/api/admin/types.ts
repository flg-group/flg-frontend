import { Number, Record, Static, String } from 'runtypes'

export const viewGameArgs = Record({ userID: Number, gameID: Number, sessionID: String })
export type viewGameArgs = Static<typeof viewGameArgs>
