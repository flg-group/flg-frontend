export type StrProp = {
  [key: string]: string
}

export type ThemeVariant = 'dark' | 'test'

export type ContainerSize = {
  medium?: number
  large?: number
  small?: number
}

export type ObjContainerSize = {
  medium?: ContainerSize
  large?: ContainerSize
  small?: ContainerSize
}

export type MediaSize = {
  medium?: string
  large?: string
  small?: string
}

export type FontSize = {
  main: number
  large: number
  small: number
  mediumsmall: number
  extrasmall: number
  huge: number
}

export type Padding = {
  medium: number
  large: number
  button: {
    task: string
    extrasmall: string
    small: string
    medium: string
    large: string
    chooseDream: string
  }
  table: {
    stocks: string
  }
  form: {
    action: string
  }
  input: {
    action: string
  }
}

export type Radius = {
  button: ContainerSize
  form: {
    action: number
  }
}

export type Color =
  | 'primary'
  | 'secondary'
  | 'green'
  | 'dark'
  | 'cyan'
  | 'blue'
  | 'red'
  | 'violet'
  | 'yellow'
  | 'darkgray'
  | 'mediumgray'
  | 'lightgray'
  | 'gray400'
  | 'gray200'
  | 'thirdly'
  | 'white'
  | 'black'
  | 'darksecondary'
  | 'gray'

export type ColorVariant = Partial<Record<Color, string>>

export type Theme = {
  container: ObjContainerSize
  breakpoints: ContainerSize
  padding: Padding
  media: MediaSize
  shadow: StrProp
  color: ColorVariant
  radius: Radius
  gradients: StrProp
  font: {
    size: FontSize
  }
}
