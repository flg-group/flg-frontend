import { allSettled } from 'effector-root'

export const bindCallAction = (scope) => async (action, params) => {
  await allSettled(action, { scope, params })
}
