import { fireEvent } from '@testing-library/react'

export const generateInputEvent = (el, body) => fireEvent.input(el, { target: { value: body } })
