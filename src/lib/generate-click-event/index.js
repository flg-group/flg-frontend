import { fireEvent } from '@testing-library/react'

export const generateClickEvent = (el) => fireEvent.click(el)
