import { defaultSort } from '../default-sort'

export const applySort = ({ order, orderBy, sortFn }, arr) => {
  return sortFn
    ? arr.sort((a, b) => sortFn({ order, orderBy, a, b }))
    : arr.sort(defaultSort({ order, orderBy }))
}
