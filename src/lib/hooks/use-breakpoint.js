import { useEffect, useState } from 'react'

import { debounce } from 'lib/debounce'
import { theme } from 'lib/theme'

const getDeviceConfig = width => {
  if (width < theme.breakpoints.tablet) {
    return 'mobile'
  } else if (width >= theme.breakpoints.tablet && width < theme.breakpoints.desktop) {
    return 'tablet'
  } else if (width >= theme.breakpoints.desktop) {
    return 'desktop'
  }
}

export const useBreakpoint = () => {
  const [brkPnt, setBrkPnt] = useState(() => getDeviceConfig(window.innerWidth))

  useEffect(() => {
    const calcInnerWidth = debounce(function() {
      setBrkPnt(getDeviceConfig(window.innerWidth))
    }, 200)
    window.addEventListener('resize', calcInnerWidth)
    return () => window.removeEventListener('resize', calcInnerWidth)
  }, [])

  return brkPnt
}
