import { useState } from 'react'

export const useRollet = (initialPos = 'up') => {
  const [rolletPos, setRolletPos] = useState(initialPos)
  const toggleRollet = () => {
    setRolletPos(rolletPos === 'down' ? 'up' : 'down')
  }
  return { rolletPos, setRolletPos, toggleRollet }
}
