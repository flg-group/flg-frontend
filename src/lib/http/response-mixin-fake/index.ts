import { AxiosResponse } from 'axios'

import { PROFILE_STATE } from 'mocks/state'

type mixinFn = (data: object | Array<any>) => object | Array<any>
export const responseMixinFake = (mixinFn: mixinFn) => (resp: AxiosResponse): AxiosResponse =>
  process.env.REACT_APP_DEV ? { ...resp, data: mixinFn(resp.data) } : resp

export const mixinRole = (role) => (data) => ({ ...data, user: { ...data.user, role } })

export const mixinProfile = (data) => {
  if (!data.profile.satisfactionLevels) {
    data.profile.satisfactionLevels = PROFILE_STATE.profile.satisfactionLevels
  }
  if (!data.profile.balance.cushion || !data.profile.balance.aggression) {
    data.profile.balance = {
      ...data.profile.balance,
      cushion: PROFILE_STATE.profile.balance.cushion,
      aggression: PROFILE_STATE.profile.balance.aggression,
    }
  }
  return data
}
