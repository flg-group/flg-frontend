export const ERROR_FINGERPRINT = 'Incorrect fingerprint, cannot authorize user session'
export const ERROR_TOKEN_BAD_FORMAT = 'Access token is badly formatted (not a uuid)'
export const ERROR_TOKEN_NOT_FOUND = 'User session with such access token was not found'
export const AUTH_ERRORS = [ERROR_FINGERPRINT, ERROR_TOKEN_BAD_FORMAT, ERROR_TOKEN_NOT_FOUND]
