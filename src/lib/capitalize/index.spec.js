import { capitalizeFirstLetter } from '.'

test('capitalizes first letter with english alphabet', () => {
  expect(capitalizeFirstLetter('vlad')).toBe('Vlad')
})

test('capitalizes first letter with russian alphabet', () => {
  expect(capitalizeFirstLetter('давид')).toBe('Давид')
})
