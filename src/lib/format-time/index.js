import { format, parseISO } from 'date-fns'

export const formatTime = (iso) => (iso ? format(parseISO(iso), 'dd.MM.yyyy HH:mm') : null)
