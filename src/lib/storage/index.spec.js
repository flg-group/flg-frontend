import { Storage } from '.'

describe('storage class', () => {
  const key = 'at'
  test('storage sets and gets access token', () => {
    Storage.setAccessToken(key)
    expect(localStorage.getItem(Storage.ACCESS_TOKEN_KEY)).toEqual(Storage.getAccessToken())
    expect(localStorage.getItem(Storage.ACCESS_TOKEN_KEY)).toBe(key)
    expect(Storage.getAccessToken()).toBe(key)
  })
  test('storage sets and gets refresh token', () => {
    Storage.setRefreshToken(key)
    expect(localStorage.getItem(Storage.REFRESH_TOKEN_KEY)).toEqual(Storage.getRefreshToken())
    expect(localStorage.getItem(Storage.REFRESH_TOKEN_KEY)).toBe(key)
    expect(Storage.getRefreshToken()).toBe(key)
  })
  test('storage sets refresh & access token, and clears storage', () => {
    Storage.setRefreshToken(key)
    Storage.setAccessToken(key)
    expect(localStorage.getItem(Storage.ACCESS_TOKEN_KEY)).toEqual(Storage.getAccessToken())
    expect(localStorage.getItem(Storage.REFRESH_TOKEN_KEY)).toEqual(Storage.getRefreshToken())
    Storage.clearStorage()
    expect(localStorage.getItem(Storage.ACCESS_TOKEN_KEY)).toBeFalsy()
    expect(localStorage.getItem(Storage.REFRESH_TOKEN_KEY)).toBeFalsy()
    expect(Storage.getAccessToken()).toBeFalsy()
    expect(Storage.getRefreshToken()).toBeFalsy()
  })
})
