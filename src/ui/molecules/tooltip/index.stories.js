import React from 'react'

import { Typography } from 'ui/atoms/typography'
import { Tooltip } from '.'

export default {
  title: 'Molecules/Tooltip',
  component: Tooltip,
  args: {
    content: 'hello!',
  },
}

const Template = (args) => {
  return (
    <Tooltip {...args}>
      <Typography>Hover on me</Typography>
    </Tooltip>
  )
}

export const TooltipStory = Template.bind({})
