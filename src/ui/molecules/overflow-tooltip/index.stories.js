import React from 'react'

import styled from 'styled-components'
import { Typography } from 'ui/atoms/typography'
import { OverflowTooltip } from 'ui/molecules/overflow-tooltip'

export default { title: 'Molecules/OverflowHint', component: OverflowTooltip }

const Inner = styled.div`
  height: 20px;
  width: 70px;
`

const Template = (args) => {
  return (
    <Inner>
      <OverflowTooltip>
        <Typography oneLine>test abcdefght</Typography>
      </OverflowTooltip>
    </Inner>
  )
}

export const OverflowTooltipStory = Template.bind({})
