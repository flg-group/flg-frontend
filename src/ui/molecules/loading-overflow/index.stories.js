import React from 'react'
import styled from 'styled-components'
import { theme } from 'theme'

import { LoadingOverflow as Loading } from '.'

export default {
  title: 'Molecules/LoadingOverflow',
  component: Loading,
  args: {
    loading: true,
    withLoader: true,
    color: theme.color.primary,
  },
}

const Box = styled.div`
  height: 400px;
  position: relative;
  width: 400px;
`

const Template = (args) => (
  <Box>
    <Loading {...args} />
  </Box>
)

export const LoadingContainer = Template.bind({})
