import React from 'react'
import { theme } from 'theme'

import { HeadingWithStripe as UIHeading } from '.'

export default {
  title: 'Molecules/HeadingWithStripe',
  component: UIHeading,
}

const Template = (args) => <UIHeading {...args} />

export const HeadingStripe = Template.bind({})

HeadingStripe.args = {
  children: 'Heading with stripe',
  fontSize: theme.font.size.large,
  stripeLeft: -30,
  stripeTop: 4,
  stripeHeight: 20,
}
