import styled from 'styled-components'
import { ifProp } from 'styled-tools'

import { theme } from 'theme'

export const Container = styled.div`
  padding: 0 ${ifProp('small', theme.padding.medium, theme.padding.large)}px;
  width: 100%;
`
