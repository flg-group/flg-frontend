import React from 'react'
import styled from 'styled-components'

import { Ticker as UITicker } from '.'

export default {
  title: 'Atoms/Ticker',
  component: UITicker,
  args: {
    children:
      'long text very long long text very long long text very long long text very long long text very long long text very long',
    speed: 1,
    gradientWidth: 30,
    showGradient: true,
    innerPadding: 4,
    bgColor: 'transparent',
    startMode: 'begin',
  },
}

const Box = styled.div`
  height: 30px;
  width: 600px;
`

const Text = styled.p`
  color: white;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`

const Template = ({ children, ...args }) => {
  return (
    <Box>
      <UITicker {...args}>
        <Text>{children}</Text>
      </UITicker>
    </Box>
  )
}

export const Ticker = Template.bind({})
