import React from 'react'
import styled from 'styled-components'

import { theme } from 'theme'

import { Input as UIInput } from '.'

export default {
  title: 'Atoms/Input',
  component: UIInput,
}

const Box = styled.div`
  width: 600px;
`

const Template = (args) => (
  <Box>
    <UIInput {...args} />
  </Box>
)

export const Input = Template.bind({})

Input.args = {
  fontSize: theme.font.size.main,
  padding: theme.padding.button.medium,
  radius: theme.radius.button.medium,
  color: theme.color.black,
  fontColor: theme.color.black,
  maxWidth: 300,
  disabled: false,
  hasError: false,
  autoSize: false,
}
