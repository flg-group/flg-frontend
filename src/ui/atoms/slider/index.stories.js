import React, { useState } from 'react'
import styled from 'styled-components'

import { Slider as UISlider } from '.'

export default {
  title: 'Atoms/Slider',
  component: UISlider,
  args: {
    max: 1,
    min: 0,
    additionalText: '',
  },
}

const Box = styled.div`
  width: 400px;
`

const Template = (args) => {
  const [x, setX] = useState(0)
  return (
    <Box>
      <UISlider {...args} value={x} onChange={setX} />
    </Box>
  )
}

export const Slider = Template.bind({})
