import { theme } from 'theme'
import styled from 'styled-components'

export const MiniCard = styled.div`
  background: ${theme.color.secondary};
  border-radius: ${theme.radius.button.medium}px;
  box-shadow: ${theme.shadow.loginForm};
  padding: 20px;
`
