import React from 'react'

import { Checkbox as UICheckbox } from '.'

export default {
  title: 'Atoms/Checkbox',
  component: UICheckbox,
}

const Template = (args) => <UICheckbox {...args} />

export const Checkbox = Template.bind({})

Checkbox.args = {
  checked: true,
}
