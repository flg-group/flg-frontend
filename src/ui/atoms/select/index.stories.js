import React, { useState } from 'react'
import styled from 'styled-components'

import { COUNTRIES } from 'lib/constants/countries'
import { hexToRGBA } from 'lib/hex-to-rgba'
import { theme } from 'theme'

import { Select as UISelect } from '.'

export default {
  title: 'Atoms/Select',
  component: UISelect,
  args: {
    options: COUNTRIES,
    color: theme.color.black,
    radius: theme.radius.button.large,
    disabled: false,
    searchable: true,
    placeholder: 'Select country pls...',
    placeholderColor: hexToRGBA(theme.color.black, 0.5),
    borderColor: hexToRGBA(theme.color.black, 0.2),
    bgColor: 'transparent',
    padding: theme.padding.button.medium,
    optionBgColor: theme.color.darkgray,
    menuListHeight: 100,
  },
}

const Box = styled.div`
  width: 600px;
`

const Template = (args) => {
  const [selected, setSelected] = useState(COUNTRIES[0].value)
  return (
    <Box>
      <UISelect {...args} value={selected} onRawChange={setSelected} />
    </Box>
  )
}

export const Select = Template.bind({})
