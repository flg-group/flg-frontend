import React from 'react'
import styled from 'styled-components'

import { Highlight as UIHighlight } from '.'

export default {
  title: 'Atoms/Highlight',
  component: UIHighlight,
}

const Box = styled.div`
  border: 1px solid rgba(255, 255, 255, 0.5);
  height: 200px;
  overflow: hidden;
  position: relative;
  text-align: center;
  width: 30%;
`

const Template = (args) => (
  <Box>
    <UIHighlight {...args} />
  </Box>
)

export const Highlight = Template.bind({})

Highlight.args = {
  forceVisibility: true,
  width: 60,
  containerOffset: -10,
  ovalOffset: -30,
  ovalOpacity: 0.7,
  ovalHeight: 30,
  ovalBlur: 25,
  type: 'bottom',
}
