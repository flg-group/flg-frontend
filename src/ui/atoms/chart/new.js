/* eslint-disable */
import { linearGradientDef } from '@nivo/core'
import { ResponsiveLine } from '@nivo/line'
import React, { useRef } from 'react'
import styled from 'styled-components'
import { prop } from 'styled-tools'
import { uuid } from 'uuidv4'

import { Warning, Logger } from 'features/error'
import { formatTooltipYMoney } from 'features/game/lib/chart-format'
import { hexToRGBA } from 'lib/hex-to-rgba'
import { CHART } from 'mocks/state'
import { theme } from 'theme'

const getColorByTrend = (data) => {
  if (data.length < 2) return
  const preLast = data[data.length - 2].value
  const last = data[data.length - 1].value
  if (preLast > last) return theme.color.red
  return theme.color.green
}

const getNormalizedData = (data, label) => {
  if (!Array.isArray(data)) {
    Logger.warn({ error: new Warning('Data passed to chart is not an array') })
    return []
  } else {
    if (data.length > 1) {
      if (data[0].value) {
        return [
          {
            id: label,
            data: data.reduce((acc, cur) => [...acc, { x: cur.turn, y: cur.value }], []),
          },
        ]
      } else if (data[0].values) {
        const labels = data[0].values.map((el) => el.label)
        const xs = data.reduce((acc, cur) => [...acc, cur.turn], [])
        //labels forEach и наполняем каждую серию итд
        let series = []
        labels.forEach((label) => {
          let labelVals = []
          data.forEach((serie, idx) => {
            const foundVal = serie.values.find((el) => el.label === label)
            labelVals.push({ y: foundVal.value, x: xs[idx] })
          })
          series.push({
            id: label,
            data: labelVals,
          })
        })
        return series
      } else {
        Logger.warn({ error: new Warning('Cannot draw chart cause of undefined data') })
        return []
      }
    } else {
      Logger.warn({ error: new Warning('Data passed to chart is length less than 2') })
      return []
    }
  }
}

const getGradientFromColor = (color, id = 'gradient') =>
  linearGradientDef(id, [
    { offset: 0, color: color, opacity: 0.75 },
    { offset: 90, color: color, opacity: 0.14 },
  ])

const getAreaBaseline = (data) => Math.min(...data.map((el) => el.value))

export const NewChart = ({
  data = CHART,
  height = 210,
  width = '100%',
  yFormatter,
  tooltipYFormatter,
  tickAmount,
  frontId = uuid(),
  label = 'Price',
  className,
}) => {
  const ref = useRef(null)
  const chartColor = getColorByTrend(data)
  const preparedData = getNormalizedData(data, label)
  let chartOpts = {
    data: preparedData,
    height,
    theme: {
      textColor: theme.color.gray400,
      grid: {
        line: {
          stroke: hexToRGBA(theme.color.black, 0.2),
        },
      },
      axis: {
        ticks: {
          line: {
            stroke: 'transparent',
          },
        },
        // legend: {
        //   text: theme.color.black,
        // },
      },
      crosshair: {
        line: {
          stroke: theme.color.primary,
          strokeWidth: 2,
          strokeOpacity: 1,
          // strokeDasharray: string
        },
      },
    },
    defs: [getGradientFromColor(theme.color.red)],
    fill: [{ match: '*', id: 'gradient' }],
    colors: [theme.color.red],
    margin: { top: 5, right: 30, bottom: 20, left: 5 },
    axisBottom: {
      orient: 'bottom',
      tickSize: 5,
      tickPadding: 5,
      tickRotation: 0,
    },
    axisRight: {
      orient: 'right',
      tickSize: 5,
      tickPadding: 5,
      tickRotation: 0,
    },
    curve: 'cardinal',
    axisTop: null,
    axisLeft: null,
    borderColor: theme.color.black,
    yScale: { type: 'linear', min: 'auto', max: 'auto', stacked: false, reverse: false },
    xScale: { type: 'linear' },
    yFormat: formatTooltipYMoney,
    // tooltip: ({point: {serieId, data: {x, yFormatted}}}) => {
    //   return <div>
    //     <Tooltip>
    //       <Label>
    //         <Typography></Typography>
    //       </Label>
    //     </Tooltip>
    //   </div>
    // },
    enableArea: true,
    areaOpacity: 1,
    areaBaselineValue: getAreaBaseline(data),
    pointSize: 0,
    pointColor: { theme: 'background' },
    pointBorderWidth: 0,
    pointBorderColor: 'green',
    pointLabel: 'y',
    pointLabelYOffset: -12,
    crosshairType: 'bottom-right',
    useMesh: true,
  }
  // chartOpts.chart.id = frontId
  // chartOpts.xaxis.tickAmount = tickAmount
  //   ? tickAmount
  //   : data.length <= 15
  //   ? 'dataPoints'
  //   : Math.floor(data.length / 2)
  return (
    <Container {...{ className, height, width }}>
      <ResponsiveLine {...chartOpts} />
    </Container>
  )
}

const Tooltip = styled.div`
  background: ${theme.color.darkgray};
  border-radius: 8px;
`

const Container = styled.div`
  height: ${prop('height')}px;
  width: ${prop('width')};
`
