import React from 'react'
import ApexCharts from 'react-apexcharts'
import styled from 'styled-components'

import { hexToRGBA } from 'lib/hex-to-rgba'
import { theme } from 'theme'

export const ColChart = ({ data, height = 350, className }) => {
  const options = {
    // colors: ['#2E93fA', '#66DA26', '#546E7A', '#E91E63', '#FF9800'],
    dataLabels: {
      enabled: false,
    },
    chart: {
      zoom: {
        enabled: false,
      },
    },
    plotOptions: {
      bar: {
        // horizontal: false,
        // startingShape: 'flat',
        // endingShape: 'flat',
        // columnWidth: '70%',
        // distributed: true,
        // rangeBarOverlap: true,
        // colors: {
        // ranges: [{
        //     from: 0,
        //     to: 0,
        //     color: undefined
        // }],
        //   backgroundBarColors: ['#2E93fA', '#66DA26', '#546E7A', '#E91E63', '#FF9800'],
        // },
      },
    },
  }
  return (
    <ChartContainer {...{ className }}>
      <ApexCharts {...{ height, options }} series={data} type="bar" />
    </ChartContainer>
  )
}

const ChartContainer = styled.div`
//   & .apexcharts-legend-marker {
//     top: 2px;
//     margin-right: 8px;
//     right: 15px;
//   }
//   & .apexcharts-tooltip-text-value {
//     font-weight: normal;
//   }
//   & .apexcharts-tooltip-text-label {
//     font-weight: 600;
//   }
//   & .apexcharts-tooltip-y-group {
//     padding-bottom: 0;
//     padding-top: 5px;
//   }
//   & .apexcharts-tooltip-marker {
//     margin-right: 6px;
//     top: 2px;
//   }
//   & .apexcharts-tooltip {
//     box-shadow: 0px 0px 7px ${hexToRGBA(theme.color.thirdly, 0.8)};
//   }
//   & .apexcharts-legend-series {
//     display: flex;
//     align-items: center;
//   }
`
