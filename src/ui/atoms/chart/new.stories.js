import React from 'react'
import styled from 'styled-components'

import { NewChart } from './new'
import { Chart } from '.'

const StyledChart = styled(NewChart)`
  margin-left: 80px;
`

const NewChartTemplate = (args) => <NewChart width="300px" {...args} />

export const NewChartStory = NewChartTemplate.bind({})

const TwoChartsTemplate = (args) => (
  <>
    <Chart {...args} />
    <StyledChart width="300px" {...args} />
  </>
)

export const CompareToOld = TwoChartsTemplate.bind({})

export default {
  title: 'Atoms/New Chart',
  component: NewChart,
}
