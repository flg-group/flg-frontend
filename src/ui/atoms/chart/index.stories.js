import React from 'react'

import { Chart } from '.'

const CurrentChartTemplate = (args) => <Chart {...args} />
export const CurrentChart = CurrentChartTemplate.bind({})
CurrentChart.args = {
  height: 210,
  label: 'Price',
}

export default {
  title: 'Atoms/Current Chart',
  component: Chart,
}
