import React from 'react'
import { Link as RouterLink } from 'react-router-dom'
import styled from 'styled-components'
import { theme } from 'theme'
import { prop } from 'styled-tools'

export const Link = ({ color = theme.color.primary, children, href }) => {
  return (
    <UILink to={href} color={color}>
      {children}
    </UILink>
  )
}

const UILink = styled(RouterLink)`
  color: ${prop('color')};
  font-size: ${theme.font.size.small}px;
  font-weight: 500;
  text-decoration: underline;
`
