import React from 'react'

import { theme } from 'theme'

import { Button as UIButton } from '.'

export default {
  title: 'Atoms/Button',
  component: UIButton,
}

const Template = (args) => <UIButton {...args} />

export const Button = Template.bind({})

Button.args = {
  variant: 'primary',
  children: 'Test button',
  radius: theme.radius.button.medium,
  padding: theme.padding.button.medium,
  fontSize: theme.font.size.main,
  fontWeight: 600,
}
