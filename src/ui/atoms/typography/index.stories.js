import React from 'react'

import { theme } from 'theme'

import { Typography as UITypography } from '.'

export default {
  title: 'Atoms/Typography',
  component: UITypography,
}

const Template = (args) => <UITypography {...args} />

export const Heading = Template.bind({})

Heading.args = {
  children: 'Test typography',
  size: theme.font.size.large,
  weight: 600,
  color: theme.color.black,
}

export const MonoFont = Template.bind({})

MonoFont.args = {
  children: '$ 100 434.32',
  size: theme.font.size.main,
  weight: 400,
  color: theme.color.black,
  fontMono: true,
}
