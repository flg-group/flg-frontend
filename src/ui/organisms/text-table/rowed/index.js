import React from 'react'
import { Typography } from 'ui/atoms/typography'
import { theme } from 'theme'
import { prop } from 'styled-tools'
import styled from 'styled-components'
import { Scrollbars } from 'ui/molecules/scrollbars'

export const RowedTextTable = ({
  rows,
  header,
  className,
  marginCols = 8,
  marginRows = 5,
  marginHeader = 10,
  renderValue = (data) => (
    <Typography weight="500" size={theme.font.size.small}>
      {data}
    </Typography>
  ),
  renderLabel = (data) => (
    <Typography color={theme.color.gray400} weight="600" size={theme.font.size.small}>
      {data}
    </Typography>
  ),
}) => {
  return (
    <Table {...{ className }}>
      {header && <Header {...{ marginHeader }}>{header}</Header>}
      <Body>
        <Scrollbars>
          {rows.map((row, idx) => (
            <Row {...{ marginCols, marginRows }} key={idx}>
              {renderLabel(row[0])}
              {renderValue(row[1])}
            </Row>
          ))}
        </Scrollbars>
      </Body>
    </Table>
  )
}

const Body = styled.div`
  flex: 1;
`

const Table = styled.div`
  //   height: 100%;
  display: flex;
  flex-direction: column;
`

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  & > *:last-child {
    margin-left: ${prop('marginCols')}px;
  }
  &:not(:last-of-type) {
    margin-bottom: ${prop('marginRows')}px;
  }
`
const Header = styled(Row)`
  margin-bottom: ${prop('marginHeader')}px;
`
