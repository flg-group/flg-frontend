import { createStore } from 'effector-root'
import { useStore } from 'effector-react'
import React from 'react'
import styled from 'styled-components'
import { uuid } from 'uuidv4'

import { createEntityListModel } from 'features/game/lib/instrument-model-factory'
import { SESSIONS_LIST } from 'mocks/rooms'

import { NewTable } from '.'

export default { title: 'Organisms/NewTable', component: NewTable }

const $sessions = createStore(SESSIONS_LIST.map((el) => ({ ...el, frontId: uuid() })))
const $sessionsImpl = createEntityListModel($sessions)

const Container = styled.div`
  height: 80%;
  overflow: hidden;
`
const Template = (args) => {
  const data = useStore($sessionsImpl.list)
  const activeRowId = useStore($sessionsImpl.selectedId)
  const sortObj = useStore($sessionsImpl.sort)
  return (
    <Container>
      <NewTable
        {...{ data, activeRowId }}
        {...sortObj}
        onChangeOrder={$sessionsImpl.setSort}
        rowIdKey="frontId"
        onRowClick={$sessionsImpl.setSelected}
        {...args}
      />
    </Container>
  )
}

export const NewTableExample = Template.bind({})
