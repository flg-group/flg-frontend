import styled from 'styled-components'

import { Typography } from 'ui/atoms/typography'

export const CellText = styled(Typography)`
  transition: color 0.3s ease-in-out;
`
