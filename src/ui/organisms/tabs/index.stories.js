import React from 'react'
import styled from 'styled-components'

import { Typography } from 'ui/atoms/typography'
import { Tabs } from '.'

export default {
  title: 'Molecules/Tabs',
  component: Tabs,
  args: {
    hasFlexBase: false,
    alignment: 'center',
  },
}

const Box = styled.div`
  width: 30%;
`

const Template = (args) => (
  <Tabs {...args}>
    <Box id="box1" title="Tab 1">
      <Typography>Tab 1 content</Typography>
    </Box>
    <Box id="box2" title="Tab 2">
      <Typography>Tab 2 content</Typography>
    </Box>
  </Tabs>
)

export const TabsStory = Template.bind({})
