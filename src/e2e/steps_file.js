/* eslint-disable */
module.exports = function () {
  const exitGame = async function () {
    this.wait(10)
    this.exitGameIfExists()
  }
  const exitGameAndLogout = async function () {
    exitGame()
    this.click('Log out')
    this.seeInCurrentUrl('login')
  }
  return actor({
    // Define custom steps here, use 'this' to access default methods of this.
    // It is recommended to place a general 'login' function here.
    login: async function (email, password) {
      this.amOnPage('/login')
      this.seeInCurrentUrl('login')
      this.fillField('email', email)
      this.fillField('password', password)
      this.click('Login')
      await this.waitForResponse((resp) => resp.url().includes('login'), 20)
      this.dontSeeInCurrentUrl('login')
    },
    exitGame,
    exitGameAndLogout,
    enterRoom: async function ({ char, roomName, email, password }) {
      this.login(email, password)
      this.exitGame()
      this.wait(1)
      this.click('$tab-training')
      this.waitForText(roomName, 30)
      this.click(roomName)
      this.click('Join')
      this.click('Registration')
      this.click('Choose')
      this.click('Choose this character')
      this.click('Play')
      this.waitInUrl('game', 30)
      this.wait(5)
    },
  })
}
