const { PLAYER_LOGIN_CREDS } = require('../common.js')
Feature('can login')

Scenario('can login for a@a.a', ({ I }) => {
  I.login(PLAYER_LOGIN_CREDS.email, PLAYER_LOGIN_CREDS.password)
})
