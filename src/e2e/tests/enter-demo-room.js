const { PLAYER_LOGIN_CREDS, DATA_TABLE_SCENARIOS } = require('../common.js')
Feature('can enter demo room')

Data(DATA_TABLE_SCENARIOS).Scenario(
  'can enter room, can end game for 2 scenarios',
  async ({ I, current }) => {
    I.login(PLAYER_LOGIN_CREDS.email, PLAYER_LOGIN_CREDS.password)
    I.exitGame()
    I.wait(1)
    I.click('$tab-training')
    I.waitForText(current, 30)
    I.click(current)
    I.click('Join')
    I.click('Registration')
    I.click('Choose')
    I.click('Choose this character')
    I.click('Play')
    I.waitInUrl('game', 30)
    I.exitGame()
  }
)
