const { LOGIN_CODE } = require('../common.js')
Feature('can login by code')

Scenario('can login by code by input for ' + LOGIN_CODE, async ({ I }) => {
  I.amOnPage('/login-by-code')
  I.fillField('code', LOGIN_CODE)
  I.click('Enter the game')
  await I.waitForResponse((resp) => resp.url().includes('login-by-code'), 10)
  I.dontSeeInCurrentUrl('/login-by-code')
})

Scenario('can login by code by link for ' + LOGIN_CODE, async ({ I }) => {
  I.amOnPage(`/login-by-code/${LOGIN_CODE}`)
  await I.waitForResponse((resp) => resp.url().includes('login-by-code'), 10)
  I.dontSeeInCurrentUrl('/login-by-code')
})
