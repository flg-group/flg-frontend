const { PLAYER_LOGIN_CREDS, DEMO_ROOM_NAME } = require('../common.js')
// Feature('step through scenario')

// Scenario.only('can enter room, can end turns to the end of the game', async (I) => {
//   I.login(PLAYER_LOGIN_CREDS.email, PLAYER_LOGIN_CREDS.password)
//   I.exitGame()
//   I.wait(2)
//   const currentUrl1 = await I.grabCurrentUrl()
//   pause({ data: currentUrl1 })
//   I.click(DEMO_ROOM_NAME, 'tr')
//   I.click('Join')
//   I.click('Registration')
//   I.click('Choose')
//   I.click('Play')
//   I.seeInCurrentUrl('game')
//   I.wait(1)
//   let currentUrl = await I.grabCurrentUrl()
//   let isSuccess = true
//   let wasError = false
//   while (!currentUrl.includes('game') && isSuccess) {
//     I.click('$end-turn-button')
//     let resp = await I.waitForResponse((resp) => resp.url().includes('transaction'), 20)
//     isSuccess = resp.body().success
//     if (!isSuccess) wasError = true
//     currentUrl = await I.grabCurrentUrl()
//   }
//   if (wasError) {
//     I.exitGame()
//   } else {
//     await I.waitInUrl('leaderboard', 5)
//     I.click('Go to rooms list')
//     await I.waitInUrl('rooms', 5)
//     I.seeInCurrentUrl('rooms')
//   }
// })
