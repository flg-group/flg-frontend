const { PLAYER_LOGIN_CREDS, DATA_TABLE_SCENARIOS, STOCK_NAME } = require('../common.js')
Feature('can do simple actions')

Scenario('can buy and sell instruments in first round', async ({ I }) => {
  I.enterRoom({
    roomName: DATA_TABLE_SCENARIOS[0],
    email: PLAYER_LOGIN_CREDS.email,
    password: PLAYER_LOGIN_CREDS.password,
  })
  I.click('$subtab-stocks')
  pause()
  I.click(STOCK_NAME)
})
