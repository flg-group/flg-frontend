exports.config = {
  tests: './tests/*.js',
  output: './output',
  helpers: {
    Playwright: {
      url: 'http://localhost:3000',
      browser: 'chromium',
      show: true,
    },
    IsInGameHelper: {
      require: './helpers/is-in-game.js',
    },
  },
  multiple: {
    basic: {
      browsers: ['chromium', 'firefox', 'webkit'],
    },
  },
  include: {
    I: './steps_file.js',
  },
  bootstrap: null,
  mocha: {},
  name: 'e2e',
  plugins: {
    retryFailedStep: {
      enabled: true,
    },
    screenshotOnFail: {
      enabled: true,
    },
    customLocator: {
      enabled: true,
      attribute: 'data-test',
      prefix: '$',
      strategy: 'css',
    },
  },
}
