const Helper = require('@codeceptjs/helper')

class IsInGameHelper extends Helper {
  async exitGameIfExists() {
    const { Playwright } = this.helpers
    const url = await Playwright.grabCurrentUrl()
    const hasActiveGame = url.includes('game')
    if (hasActiveGame) {
      Playwright.click('$turn-actions-button')
      Playwright.click('$end-game-button')
      Playwright.click('Confirm')
      await Playwright.waitInUrl('leaderboard', 30)
      Playwright.click('Go to rooms list')
      await Playwright.waitInUrl('rooms', 30)
    }
  }
}

module.exports = IsInGameHelper
