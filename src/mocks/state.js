import { theme } from 'theme'
import GameStateNormalized from './game/state/normalized.json'
import GameStateRaw from './game/state/raw.json'

const DAY = 1000 * 60 * 60 * 24
const YEAR = DAY * 365
export const GAME_STATE_NORMALIZED = GameStateNormalized
export const GAME_STATE_RAW = GameStateRaw
export const CHART = [
  {
    date: new Date(Number(new Date()) - YEAR * 12).toISOString(),
    turn: 0,
    value: 100,
  },
  {
    date: new Date(Number(new Date()) - YEAR * 11).toISOString(),
    turn: 1,
    value: 142.1,
  },
  {
    date: new Date(Number(new Date()) - YEAR * 10).toISOString(),
    turn: 2,
    value: 132.9,
  },
  {
    date: new Date(Number(new Date()) - YEAR * 9).toISOString(),
    turn: 3,
    value: 140,
  },
  {
    date: new Date(Number(new Date()) - YEAR * 8).toISOString(),
    turn: 4,
    value: 139.34,
  },
  {
    date: new Date(Number(new Date()) - YEAR * 7).toISOString(),
    turn: 5,
    value: 110,
  },
  {
    date: new Date(Number(new Date()) - YEAR * 6).toISOString(),
    turn: 6,
    value: 124,
  },
  {
    date: new Date(Number(new Date()) - YEAR * 5).toISOString(),
    turn: 7,
    value: 190,
  },
  {
    date: new Date(Number(new Date()) - YEAR * 4).toISOString(),
    turn: 8,
    value: 186,
  },
  {
    date: new Date(Number(new Date()) - YEAR * 3).toISOString(),
    turn: 9,
    value: 174.32,
  },
  {
    date: new Date(Number(new Date()) - YEAR * 2).toISOString(),
    turn: 10,
    value: 189,
  },
  {
    date: new Date(Number(new Date()) - YEAR).toISOString(),
    turn: 11,
    value: 167.01,
  },
  {
    date: new Date().toISOString(),
    turn: 12,
    value: 200.42,
  },
]

export const getRandomChart = () => {
  let length = Number((Math.random() * 30 + 10).toFixed(0))
  return new Array(length).fill(length).map((_, idx) => ({
    date: new Date(Number(new Date()) - YEAR * (length - idx - 1)).toISOString(),
    value: +(Math.random() * 300 + Math.random() * 150).toFixed(2),
  }))
}

export const getRandomPrice = (multiplicator = 1) => +(20 + Math.random() * 100 * multiplicator).toFixed(2)

export const INSTRUMENT_PROTOTYPE = {
  id: 'aa',
  name: 'Apple',
  price: {
    current: getRandomPrice(),
    previous: getRandomPrice(),
  },
  image: 'https://upload.wikimedia.org/wikipedia/commons/3/31/Apple_logo_white.svg',
  news: [
    {
      name: 'News Title',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
      image: null,
      date: new Date(Number(new Date()) - YEAR).toISOString(),
    },
  ],
  charts: {
    price: getRandomChart(),
    revenue: getRandomChart(),
  },
  about: {
    description: 'lorem lorem',
  },
}

export const MOCKED_EVENT = {
  id: 0,
  name: 'Test',
  image: null,
  description: 'Test event with reactions',
  type: 'income',
  sum: 7500,
  status: 'appointed',
  actions: [
    {
      id: 0,
      name: 'Do something positive',
      type: 'positive',
    },
    {
      id: 1,
      name: 'Do smth neg',
      type: 'negative',
    },
    {
      id: 2,
      name: 'wow!',
    },
  ],
}

export const PROFILE_STATE = {
  finishTime: new Date(Number(new Date()) + 1000 * 60 * 60 * 2).toISOString(),
  currentTurn: 23,
  turnQty: 45,
  profile: {
    id: 'donald', // айди персонажа, а не игрока
    age: 45,
    balance: {
      cash: {
        previous: 400,
        current: 600,
      },
      capital: {
        previous: 77000,
        current: 74000,
      },
      flowBonus: {
        previous: 0.2,
        current: 0.5,
      },
      endBonus: {
        previous: 0.4,
        current: 0.1,
      },
      points: {
        previous: 2500,
        current: 2600,
      },
      revenue: {
        previous: 7500,
        current: 6000,
      },
      profit: {
        previous: 7500,
        current: 6000,
      },
      expenses: {
        previous: 1213,
        current: 3455,
      },
      cushion: 0.7,
      aggression: [
        {
          description: 'Sber ETFs',
          value: 0.5,
        },
        {
          description: 'Leverage used for Sber ETFs',
          value: 0.2,
        },
        {
          description: 'REIT',
          value: 0.3,
          color: theme.color.cyan,
        },
      ],
    },
    bonuses: {
      endBonus: [{ description: 'Financial studies', value: 0.1 }],
      flowBonus: [{ description: 'Standard of a living', value: 0.5 }],
    },
    satisfactionLevels: {
      vacation: 0.3,
      medicine: 0.7,
      entertainment: 0.17,
      fitness: 0.95,
    },
    charts: {
      expenses: getRandomChart(),
      income: getRandomChart(),
      flowBonus: getRandomChart(),
      endBonus: getRandomChart(),
    },
    news: [
      {
        // глобальные новости мира и экономики
        id: 'aa',
        name: 'Global pandemic! ☣️',
        description: '<i>Not great, not terrible.</i>',
        image: 'https://img.jakpost.net/c/2020/03/12/2020_03_12_89013_1583994402._large.jpg',
      },
      {
        // глобальные новости мира и экономики
        id: 'aa',
        name: 'Russia has started a WWIII',
        description: '<b>bold textt</b>',
        image: 'https://www.meme-arsenal.com/memes/0ea85ecca5b794c7f3e5565031372dd0.jpg',
      },
    ],
    events: [
      {
        id: 'dfdd',
        name: "You're sick.",
        image: '',
        description: '',
        type: 'sell', // 'sell' || 'buy'
        sum: 500,
        status: 'appointed', // 'appointed' || 'processing' || 'finished' || 'aborted'
      },
      MOCKED_EVENT,
    ],
  },
}

export const MARKET = {
  stockMarket: {
    stocks: [
      {
        id: 'aa',
        ticker: 'AAPL',
        name: 'Apple',
        price: {
          current: getRandomPrice(),
          previous: getRandomPrice(),
        },
        image: 'https://upload.wikimedia.org/wikipedia/commons/3/31/Apple_logo_white.svg',
        dividends: 12.2,
        leverage: 7,
        news: [
          {
            name: 'News Title',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
        ],
        charts: {
          price: getRandomChart(),
          revenue: getRandomChart(),
        },
        about: {
          description: 'lorem lorem',
        },
      },
      {
        id: 'rosneft',
        ticker: 'ROSN',
        name: 'Rosneft',
        price: {
          current: getRandomPrice(),
          previous: getRandomPrice(),
        },
        image: 'https://upload.wikimedia.org/wikipedia/commons/3/39/Rosneft_logo.svg',
        dividends: 0.1,
        leverage: 7,
        news: [
          {
            name: 'News Title veryyy biig title omg its very long heading name newds wwooow such long',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
          {
            name: 'News Title veryyy biig title omg its very long heading name news wwooow such long',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
          {
            name: 'News Title veryyy biig title omg its very losddfsdng heading name news wwooow such long',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
          {
            name:
              'News Title veryyy biig title omg its versdfsdfy losddfsdng heading name news wwooow such long',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
        ],
        charts: {
          price: getRandomChart(),
          revenue: getRandomChart(),
        },
        about: {
          description: 'lorem rich text ok <i>italic</i>',
        },
      },
      {
        id: 'rosnef1',
        ticker: 'ROS1',
        name: 'Royal Caribbean Cruises America Ltd',
        price: {
          current: getRandomPrice(),
          previous: getRandomPrice(),
        },
        image: 'https://upload.wikimedia.org/wikipedia/commons/3/39/Rosneft_logo.svg',
        dividends: 43,
        leverage: 2,
        news: [
          {
            name: 'News Title',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
        ],
        charts: {
          price: getRandomChart(),
          revenue: getRandomChart(),
        },
        about: {
          description: 'lorem rich text ok <i>italic</i>',
        },
      },
      {
        id: 'rosnano',
        ticker: 'RSNO',
        name: 'Rosnano',
        price: {
          current: getRandomPrice(),
          previous: getRandomPrice(),
        },
        image: null,
        dividends: 150,
        leverage: 2,
        news: [
          {
            name: 'News Title',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
        ],
        charts: {
          price: getRandomChart(),
          revenue: getRandomChart(),
        },
        about: {
          description: 'lorem rich text ok <i>italic</i>',
        },
      },
    ],
    bonds: [
      {
        id: 'aa',
        ticker: 'AAPL',
        name: 'Apple',
        price: {
          current: getRandomPrice(),
          previous: getRandomPrice(),
        },
        image: 'https://upload.wikimedia.org/wikipedia/commons/3/31/Apple_logo_white.svg',
        dividends: 12.2,
        leverage: 7,
        expirationTurn: 23,
        discountOnPreSale: 0.7,
        news: [
          {
            name: 'News Title',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
        ],
        charts: {
          price: getRandomChart(),
          revenue: getRandomChart(),
        },
        about: {
          description: 'lorem rich text ok <i>italic</i>',
        },
      },
      {
        id: 'rosneft',
        ticker: 'ROSN',
        name: 'Rosneft',
        price: {
          current: getRandomPrice(),
          previous: getRandomPrice(),
        },
        image: 'https://upload.wikimedia.org/wikipedia/commons/3/39/Rosneft_logo.svg',
        dividends: 20,
        leverage: 7,
        expirationTurn: 27,
        discountOnPreSale: 0.7,
        news: [
          {
            name: 'News Title',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
        ],
        charts: {
          price: getRandomChart(),
          revenue: getRandomChart(),
        },
        about: {
          description: 'lorem rich text ok <i>italic</i>',
        },
      },
    ],
    indicies: [
      {
        id: 'sp500',
        ticker: 'SPX',
        name: 'S&P 500',
        price: {
          current: getRandomPrice(),
          previous: getRandomPrice(),
        },
        news: [
          {
            name: 'News Title',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
        ],
        charts: {
          price: getRandomChart(),
        },
        about: {
          description: 'lorem rich text ok <i>italic</i>',
        },
      },
      {
        id: 'dow',
        ticker: 'DOW',
        name: 'Dow',
        price: {
          current: getRandomPrice(),
          previous: getRandomPrice(),
        },
        news: [
          {
            name: 'News Title',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
        ],
        charts: {
          price: getRandomChart(),
        },
        about: {
          description: 'lorem rich text ok <i>italic</i>',
        },
      },
    ],
    commodities: [
      {
        id: 'silver',
        name: 'Silver',
        price: {
          current: getRandomPrice(),
          previous: getRandomPrice(),
        },
        news: [
          {
            name: 'News Title',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
        ],
        charts: {
          price: getRandomChart(),
        },
        about: {
          description: 'lorem rich text ok <i>italic</i>',
        },
      },
      {
        id: 'gold',
        name: 'Gold',
        price: {
          current: getRandomPrice(),
          previous: getRandomPrice(),
        },
        news: [
          {
            name: 'News Title',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
        ],
        charts: {
          price: getRandomChart(),
        },
        about: {
          description: 'lorem rich text ok <i>italic</i>',
        },
      },
    ],
    etf: [
      {
        id: 'etf1',
        dividends: 2,
        name: 'ETF Sberbank',
        price: {
          current: getRandomPrice(),
          previous: getRandomPrice(),
        },
        leverage: 4,
        news: [
          {
            name: 'News Title',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
        ],
        charts: {
          price: getRandomChart(),
        },
        about: {
          description: 'lorem rich text ok <i>italic</i>',
        },
      },
      {
        id: 'etf2',
        dividends: 342,
        name: 'ETF 2',
        price: {
          current: getRandomPrice(),
          previous: getRandomPrice(),
        },
        leverage: 4,
        news: [
          {
            name: 'News Title',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
        ],
        charts: {
          price: getRandomChart(),
        },
        about: {
          description: 'lorem rich text ok <i>italic</i>',
        },
      },
    ],
    mf: [
      {
        id: 'mf1',
        dividends: 2,
        name: 'MF Sberbank',
        price: {
          current: getRandomPrice(),
          previous: getRandomPrice(),
        },
        leverage: 2,
        news: [
          {
            name: 'News Title',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
        ],
        charts: {
          price: getRandomChart(),
        },
        about: {
          description: 'lorem rich text ok <i>italic</i>',
        },
      },
      {
        id: 'mf2',
        dividends: 342,
        name: 'MF 2',
        price: {
          current: getRandomPrice(),
          previous: getRandomPrice(),
        },
        leverage: 2,
        news: [
          {
            name: 'News Title',
            description:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget',
            image: null,
            date: new Date(Number(new Date()) - YEAR).toISOString(),
          },
        ],
        charts: {
          price: getRandomChart(),
        },
        about: {
          description: 'lorem rich text ok <i>italic</i>',
        },
      },
    ],
  },
  dreamsAndGoals: {
    dreams: [
      {
        id: 0,
        name: 'VW Polo Sedan 2017 Trendline',
        description: `<div class="StyledAccordion-cMFgHY fCYRYR" id="sectiongroup_214360841_highlightfeaturesect_item_0"><button class="StyledAccordionHead-YPAXu glooJZ" id="sectiongroup_214360841_highlightfeaturesect_item_0_head" tabindex="0" aria-expanded="true" aria-controls="sectiongroup_214360841_highlightfeaturesect_item_0_body"><div class="StyledOverflowWrapper-sc-mfnmbg RijZV" wrap="never"><div class="StyledContainer-sc-18harj2 dCycse" wrap="never"><div class="StyledChildWrapper-sc-1d21nde bNlDhf" wrap="never"><div class="StyledTextComponent-sc-hqqa9q dmbbLl"><div class="richtextSimpleElement StyledEditableComponent-cPQnBH fVLIhM"><span class="StyledTextComponent-sc-hqqa9q dmbbLl"><span class="sc-EHOje hbKBHO">Экстерьер</span></span></div></div></div><div class="StyledChildWrapper-sc-1d21nde bNlDhf" wrap="never"><div class="StyledIconWrapper-hBqUFs cjDKdm" direction="ltr"></div></div></div></div></button><div class="StyledAccordionBody-cDcVhI dSaEJA" id="sectiongroup_214360841_highlightfeaturesect_item_0_body" aria-hidden="false" aria-labelledby="sectiongroup_214360841_highlightfeaturesect_item_0_head" height="168"><div class="StyledAccordionBodyContent-hfkczr bCrPVB"><div class="StyledOverflowWrapper-sc-mfnmbg RijZV" wrap="always"><div class="StyledContainer-sc-18harj2 iKaJra" wrap="always"><div class="StyledChildWrapper-sc-1d21nde YGOiL" wrap="always"><div class="headingElement StyledEditableComponent-cPQnBH fVLIhM"><h2 class="StyledTextComponent-sc-hqqa9q bcQufd"><span class="sc-EHOje jLFYhM">Ваши глаза вас <b>не обманывают</b></span></h2></div></div><div class="StyledChildWrapper-sc-1d21nde YGOiL" wrap="always"><div class="StyledGalleryWrapper-eaWFOk cmoIRV"><div class="StyledGalleryWrapper-eWylkB eLNoes"><div class="sc-iGrrsa iwtYew"><div class="sc-ebFjAB gnskmX"><div class="sc-bdVaJa jzpEZD"><ul style="transform:translate3d(0%, 0, 0);transition:0.3s cubic-bezier(0.14, 1.12, 0.67, 0.99)" class="sc-htpNat jLIXaa" direction="ltr"><li style="opacity:1" class="sc-bwzfXH jLjXvt"><div class="mediaElement StyledEditableComponent-cPQnBH fVLIhM"><div class="StyledItemOverlayWrapper-iHoass biYHKZ"><div class="imageElement StyledEditableComponent-cPQnBH fVLIhM"><div class="StyledWrapper-sc-iw10kj YnRcv"><div class="StyledImageDisclaimerWrapper-jssUUa jxUPGG"><div class="sc-kTUwUJ bJXqSX"><noscript></noscript><img class="lazyload  Image-sc-3anbxi cUKAQM" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9anBnJnFsdD03OSZ3aWQ9ODAwJmhlaT00NTAmYWxpZ249MC4wMCwwLjAwJmRhMTk=" data-srcset="https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9anBnJnFsdD03OSZ3aWQ9MzIwJmhlaT0xODAmYWxpZ249MC4wMCwwLjAwJjhiYzA= 320w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9anBnJnFsdD03OSZ3aWQ9NTAwJmhlaT0yODEmYWxpZ249MC4wMCwwLjAwJjI2ZDY= 500w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9anBnJnFsdD03OSZ3aWQ9NjgwJmhlaT0zODMmYWxpZ249MC4wMCwwLjAwJjcxYTI= 680w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9anBnJnFsdD03OSZ3aWQ9ODAwJmhlaT00NTAmYWxpZ249MC4wMCwwLjAwJmRhMTk= 800w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9anBnJnFsdD03OSZ3aWQ9OTYwJmhlaT01NDAmYWxpZ249MC4wMCwwLjAwJjQ5ZGE= 960w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9anBnJnFsdD03OSZ3aWQ9MTI4MCZoZWk9NzIwJmFsaWduPTAuMDAsMC4wMCY4YjU2 1280w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9anBnJnFsdD03OSZ3aWQ9MTI4MCZoZWk9NzIwJmFsaWduPTAuMDAsMC4wMCY4YjU2 1920w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9anBnJnFsdD03OSZ3aWQ9MTI4MCZoZWk9NzIwJmFsaWduPTAuMDAsMC4wMCY4YjU2 2400w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9anBnJnFsdD03OSZ3aWQ9MTI4MCZoZWk9NzIwJmFsaWduPTAuMDAsMC4wMCY4YjU2 2880w" alt="" data-srcset-webp="https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9d2VicCZxbHQ9Nzkmd2lkPTMyMCZoZWk9MTgwJmFsaWduPTAuMDAsMC4wMCZiZDc3 320w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9d2VicCZxbHQ9Nzkmd2lkPTUwMCZoZWk9MjgxJmFsaWduPTAuMDAsMC4wMCY1ODhk 500w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9d2VicCZxbHQ9Nzkmd2lkPTY4MCZoZWk9MzgzJmFsaWduPTAuMDAsMC4wMCZhMzU5 680w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9d2VicCZxbHQ9Nzkmd2lkPTgwMCZoZWk9NDUwJmFsaWduPTAuMDAsMC4wMCYwYmQw 800w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9d2VicCZxbHQ9Nzkmd2lkPTk2MCZoZWk9NTQwJmFsaWduPTAuMDAsMC4wMCY3Yjkx 960w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9d2VicCZxbHQ9Nzkmd2lkPTEyODAmaGVpPTcyMCZhbGlnbj0wLjAwLDAuMDAmZmJmYg== 1280w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9d2VicCZxbHQ9Nzkmd2lkPTEyODAmaGVpPTcyMCZhbGlnbj0wLjAwLDAuMDAmZmJmYg== 1920w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9d2VicCZxbHQ9Nzkmd2lkPTEyODAmaGVpPTcyMCZhbGlnbj0wLjAwLDAuMDAmZmJmYg== 2400w,https://s7g10.scene7.com/is/image/volkswagenag/polo_t4-1?Zml0PWNyb3AsMSZmbXQ9d2VicCZxbHQ9Nzkmd2lkPTEyODAmaGVpPTcyMCZhbGlnbj0wLjAwLDAuMDAmZmJmYg== 2880w"></div></div></div></div></div></div></li></ul></div></div><div class="sc-dEoRIm ZXTYa"><div class="sc-jKVCRD juBEbe" direction="ltr"><div class="StyledTextComponent-sc-hqqa9q hMSGYU"><div class="richtextSimpleElement StyledEditableComponent-cPQnBH fVLIhM"></div></div></div></div></div></div></div></div></div></div><div class="StyledOverflowWrapper-sc-mfnmbg RijZV" wrap="always"><div class="StyledContainer-sc-18harj2 bStPJW" wrap="always"><div class="StyledChildWrapper-sc-1d21nde cScOMB" wrap="always"><div class="StyledCopyItem-jwHlCa gVlKDx"><div class="copyItem StyledEditableComponent-cPQnBH fVLIhM"><div class="StyledBodyWrapper-hZMKrF gyNdSv"><div class="StyledTextComponent-sc-hqqa9q bHbxdi"><span class="sc-EHOje hbKBHO"><p>Седан <span class="StyledSafeWord-gZTXrG cqbDfK">Volkswagen</span> Polo действительно настолько хорош, насколько Вам показалось с первого взгляда. Как снаружи, так и внутри.</p></span></div></div></div></div></div></div></div></div></div></div>`,
        image:
          'https://2.bp.blogspot.com/-be6NzBDqikA/XlZlxM38HEI/AAAAAAAAaZg/7ulGnhvxdSM-z9cV6MoGUZl2jmLtK6APgCLcBGAsYHQ/s1600/1.jpg',
        sum: 12000,
        endBonus: 0.12,
        tasks: [
          {
            id: 0,
            subjectID: 'silver', //id той сущности, которую нужно купить
            subjectType: 'commodities', // 're' || 'product' || 'service' || 'etf' || 'stock' etc...
          },
          {
            id: 1,
            subjectID: 'aa', //id той сущности, которую нужно купить
            subjectType: 'bonds', // 're' || 'product' || 'service' || 'etf' || 'stock' etc...
          },
        ],
      },
      {
        id: 1,
        name: 'Cozy 2BR in Atlanta',
        description: `<p class="sc-fzolEj fKOJYi">The condo unit is a beachfront property with an awesome view of the beach resort. It's a 2-bedroom(130sqm) fully furnished unit with balcony located at the 2nd flr of a 3-storey apartment facing the beach and condo pool.<br>Overnite rate with beach access.  NO entrance fee and FREE Parking!<br>** 14,000 per nite for 8 pax<br>**  1000 per additional person per nite<br>** Plus 600 cleaning fee (refundable if you leave the unit clean<br>( 15 guests is the ideal maximum we can accommodate in the unit)<br><br>1) Two-bedrooms - queen size beds with extra queen size floor mattresses, both rooms are air conditioned with own toilet and bath.<br>2) Living and dining room - spacious area with sofa and cable tv, 8 seater dining table, air conditioned, overlooking the pool and beach.<br>3) 2 toilet and bath with water heater, bidet, basic toiletries (note: towels are not provided) we stopped providing towels due to hygienic reasons).<br>4) Kitchen -stove with oven, hot &amp; cold water dispenser,  drinking water, ref, microwave, rice cooker, toaster, kettle, cooking utensils, dinnerware ( plates, glasses, cups, fork &amp; spoon), dishwashing liquid<br>5) balcony - 6 seater coffee table overlooking the resort<br><br>We provide limited pillows and blankets. If you are a group of more than 12 pax please bring extra beddings for your comfort.<br></p>`,
        image:
          'https://q-xx.bstatic.com/xdata/images/hotel/840x460/168276422.jpg?k=02567db012dea7bf4453bcc635ef633bf404e88583a69a33b7704ffc4324d885&o=',
        sum: 380000,
        endBonus: 0.8,
        tasks: [
          {
            id: 0,
            subjectID: 'gold', //id той сущности, которую нужно купить
            subjectType: 'commodities', // 're' || 'product' || 'service' || 'etf' || 'stock' etc...
          },
          {
            id: 1,
            subjectID: 'aa', //id той сущности, которую нужно купить
            subjectType: 'stocks', // 're' || 'product' || 'service' || 'etf' || 'stock' etc...
          },
        ],
      },
    ],
    goals: [
      {
        id: 0,
        name: 'Goal 1',
        description: 'desc 1',
        image:
          'https://specials-images.forbesimg.com/imageserve/5deac8c3b269e900075d9921/960x0.jpg?fit=scale',
        sum: 15000,
        flowBonus: 0.4,
        tasks: [
          {
            id: 0,
            subjectID: 'aa', //id той сущности, которую нужно купить
            subjectType: 'stocks', // 're' || 'product' || 'service' || 'etfmf' || 'stock' etc...
          },
          {
            id: 1,
            subjectID: 'rosneft', //id той сущности, которую нужно купить
            subjectType: 'bonds', // 're' || 'product' || 'service' || 'etfmf' || 'stock' etc...
          },
        ],
      },
      {
        id: 1,
        name: 'Goal 2',
        description: 'desc 2',
        image: 'https://cdn2.hubspot.net/hubfs/1686933/stock%20images-new/goal%20bullseye.png',
        sum: 30000,
        flowBonus: 0.1,
        tasks: [],
      },
    ],
  },
  goodsAndServices: {
    realEstate: [
      {
        name: '2BR in Atlanta',
        image:
          'https://q-xx.bstatic.com/xdata/images/hotel/840x460/168276422.jpg?k=02567db012dea7bf4453bcc635ef633bf404e88583a69a33b7704ffc4324d885&o=',
        creditDetails: {
          // детали по ипотеке, если её можно взять
          rate: 0.12,
          minDownPayment: 5000,
          minYear: 3,
          maxYear: 30,
        },
        bonuses: {
          buying: {
            flowBonus: 1.2,
            endBonus: 0.6,
          },
          rent: {
            flowBonus: 0.05,
          },
        },
        description: 'cool flat bro',
        price: {
          value: 145300,
          prevValue: 147000,
        }, // цена покупки
        rent: {
          value: 850,
          prevValue: 1000,
        }, // цена аренды
        news: [],
        charts: {
          price: getRandomChart(),
        },
        relatedGoalID: null, // если есть задача в целях персонажа
        relatedDreamID: 1,
      },
    ],
    goods: [
      {
        id: 0,
        name: 'Volkswagen Polo Sedan 2017 Trendline',
        image: 'https://img1.autospot.ru/resize/800x-/volkswagen/polo/744801/',
        creditDetails: {
          rate: 0.05,
          minDownPayment: 2000,
          minYear: 1,
          maxYear: 10,
        },
        bonuses: {
          selling: {
            flowBonus: -0.1,
            endBonus: -0.1,
          },
          buying: {
            flowBonus: 1.4,
            endBonus: 0.2,
          },
        },
        description: 'cool car!',
        price: {
          current: 13500,
          previous: 10000,
        },
        news: [],
        charts: {
          price: getRandomChart(),
        },
        relatedGoalID: null,
        relatedDreamID: 0,
      },
      {
        id: 1,
        name: 'iPhone 12 Pro Uber Max',
        image: 'https://www.ixbt.com/img/n1/news/2020/4/1/11.05_large.jpg',
        creditDetails: {
          rate: 0.2,
          minDownPayment: 50,
          minYear: 1,
          maxYear: 5,
        },
        bonuses: {
          selling: {
            flowBonus: -0.1,
            endBonus: null,
          },
          buying: {
            flowBonus: 0.002,
            endBonus: null,
          },
        },
        description: 'iphone 12 is cool, buy it!',
        price: {
          current: 2000,
          previous: 1700,
        },
        news: [
          {
            name: 'Steve Jobs is risen!',
            description: 'omg',
            image: null,
            date: null,
          },
        ],
        charts: {
          price: getRandomChart(),
        },
        relatedGoalID: null,
        relatedDreamID: null,
      },
    ],
    services: [
      {
        id: 0,
        name: 'Barber',
        image:
          'https://avatars.mds.yandex.net/get-zen_doc/61014/pub_5b8fef8567918d00aa707405_5b8fef951c6ec800aebab784/scale_1200',
        startTurn: 0,
        expirationTurn: 5,
        creditDetails: null,
        bonuses: {
          buying: {
            flowBonus: 0.01,
            endBonus: 0.1,
            otherBonuses: ['+25% to your niceness'],
          },
        },
        description: 'barbershop for 5 years!',
        price: {
          current: 50,
          previous: 45,
        },
        charts: {
          price: getRandomChart(),
        },
        relatedGoalID: 0,
        relatedDreamID: null,
      },
    ],
  },
}

export const PORTFOLIO = {
  stockMarket: {
    stocks: [
      {
        id: 'aa',
        qty: 14,
      },
      {
        id: 'rosnef1',
        qty: 2,
        creditDetails: {
          usedSum: 1240,
          shoulder: 2,
          rest: 500,
        },
      },
    ],
  },
  banking: {
    leverage: {
      used: 1240, // использовано
    },
  },
  dreamsAndGoals: {
    dreams: [
      {
        id: 0,
        status: 'appointed', // 'ready' || 'processing' || 'notReady'
        tasks: [
          {
            id: 0,
            status: 'notReady', // 'ready' || 'processing' || 'notReady'
          },
          {
            id: 1,
            status: 'ready',
          },
        ],
      },
      {
        id: 1,
        status: 'processing', // 'ready' || 'processing' || 'notReady'
        tasks: [],
      },
    ],
    goals: [
      {
        id: 0,
        status: 'aborted', // 'ready' || 'processing' || 'notReady'
        tasks: [
          {
            id: 0,
            status: 'notReady', // 'ready' || 'processing' || 'notReady'
          },
          {
            id: 1,
            status: 'ready',
          },
        ],
      },
      {
        id: 1,
        status: 'processing', // 'ready' || 'processing' || 'notReady'
        tasks: [],
      },
    ],
  },
  goodsAndServices: {
    goods: [
      {
        id: 1,
        qty: 1,
        creditDetails: {
          sum: 2000,
          rest: 1600,
          rate: 0.12,
          downPayment: 500,
          years: 3,
        },
      },
    ],
    services: [],
  },
}
